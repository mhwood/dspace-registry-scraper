/*
 * Copyright 2018 Mark H. Wood.
 */

package com.markhwood.dspace.registry;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.ds.common.BaseDataSource;

/**
 * Given a CSV file of Registry records, check each site's registered DSpace
 * version against the version that the site reports via the Generator HTTP
 * header.  The Registry data are found in the file named by the first command
 * line argument.  The report is written to standard output.
 *
 * @author mhwood
 */
public class RegistryChecker
{
    /**
     * Read DSpace Registry records as CSV; write a report on versions.
     *
     * @param argv command line arguments.
     */
    public static void main(String[] argv)
    {
        DataSource db = new PGSimpleDataSource();
        ((BaseDataSource) db).setDatabaseName("dspace_registry");

        ResultSet records = null;
        try {
            Connection dbConnection = db.getConnection();
            Statement statement = dbConnection.createStatement(
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            records = statement.executeQuery(
                    "SELECT institution_name AS name,"
                    + " dspace_version AS version,"
                    + " repo_url AS url"
                    + " FROM instance");
            while (records.next())
            {
                String siteName = records.getString("name");
                String siteVersion = records.getString("version");
                String siteUrl = records.getString("url");

                System.out.println(siteName);
                System.out.format("  Registered version:  %s%n", siteVersion);

                // Some sites don't register a URL.
                if (null == siteUrl || siteUrl.isEmpty())
                {
                    System.out.println("  WARNING:  No URL registered.");
                    continue;
                }

                // Fetch the page and see if it has a Generator meta element.
                String generator = null;
                try {
                    Document page = (Document) Jsoup.connect(siteUrl)
                            .timeout(5 * 1000)
                            .get();
                    Element generatorMeta = page.selectFirst("meta[name='Generator']");
                    if (null == generatorMeta)
                    {
                        System.out.println("  WARNING:  No Generator META -- older than 4.0");
                        continue;
                    }
                    generator = generatorMeta.attr("content");
                } catch (MalformedURLException ex) {
                    System.err.format("  ERROR:  Problem with URL %s: %s%n",
                            siteUrl, ex.getMessage());
                    continue;
                } catch (ProtocolException ex) {
                    System.err.format("  ERROR:  ProtocolException:  %s%n", ex.getMessage());
                    System.exit(1);
                } catch (IOException ex) {
                    System.err.format("  ERROR:  IOException:  %s%n", ex.getMessage());
                    continue;
                }

                if (null == generator)
                {
                    System.out.println("  WARNING:  No Generator -- older than 4.0");
                    continue;
                }

                // DSpace since 4.0 sets META Generator=DSpace version.
                System.out.format("  Actual version:  %s%n", generator);
            }
            records.close();
        } catch (SQLException ex) {
            System.err.format("  ERROR:  %s%n", ex.getMessage());
            System.exit(1);
        }
    }

    /** Don't instantiate this class! */
    private RegistryChecker(){};
}
