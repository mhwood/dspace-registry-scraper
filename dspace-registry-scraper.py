#! /usr/bin/python3.5
# Copyright 2018 Indiana University
# Mark H. Wood, June 2018

# Scrape basic information from the DSpace Registry at the Duraspace website
# and stash the data in a relational database for later.

import sys
from http.client import HTTPConnection, HTTPResponse
from bs4 import BeautifulSoup
import csv

# Connect to the Duraspace DSpace instances registry.
connection = HTTPConnection('registry.duraspace.org')
page_url = '/registry/dspace'

# Set up to write CSV to standard output.
# not until 3.7 sys.stdout.reconfigure(newline='')
fieldnames = ['name', 'type', 'country', 'version', 'url']
writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
writer.writeheader()

# Scrape each page
while page_url is not None :
    # Fetch and parse the page.
    connection.request('GET', page_url)
    soup = BeautifulSoup(connection.getresponse(), 'html.parser')

    # Find the table of instances.
    section = soup.find('section', id='block-system-main')
    table = section.find('table')
    body = table.find('tbody')
    rows = body.find_all('tr')

    # Process each DSpace instance.
    for row in rows:
        # Extract cell values.
        institution_name = row.find_all(name='td', class_='views-field-field-institution')[0].a.get_text().strip()
        #institution_detail_url = row.find_all(name='td', class_='views-field-field-institution')[0].a['href'].strip()
        institution_type = row.find_all(name='td', class_='views-field-field-institution-type')[0].get_text().strip()
        country = row.find_all(name='td', class_='views-field-field-country')[0].get_text().strip()
        dspace_version = row.find_all(name='td', class_='views-field-field-dspace-version')[0].get_text().strip()
        repo_url_link = row.find_all(name='td', class_='views-field-field-repository-url')[0].a

        # Some rows lack a repository link.  Avoid nullity.
        if repo_url_link is not None :
            repo_url = repo_url_link['href'].strip()
        else :
            repo_url = None

        # Write it all out.
        writer.writerow({'name': institution_name,
                         'type': institution_type,
                         'country': country,
                         'version': dspace_version,
                         'url': repo_url})

    # Find the link to the next page.
    page_menu = soup.find('div', class_='pagination');
    page_next = page_menu.find('a', class_='next');
    if page_next is not None :
        page_url = page_next['href']
# DEBUG        print('next-page URL: ', page_url, file=sys.stderr)
    else :
        page_url = None

# All pages have been seen.
print("That's all!", file=sys.stderr)
