CREATE SEQUENCE IF NOT EXISTS instance_seq AS INTEGER;
CREATE TABLE instance (
	id INTEGER PRIMARY KEY DEFAULT nextval('instance_seq'),
	institution_name TEXT,
	institution_type TEXT,
	country TEXT,
	dspace_version TEXT,
	repo_url TEXT
);
