#! /usr/bin/python3.6
# Copyright 2018 Indiana University
# Mark H. Wood, June 2018

# Scrape basic information from the DSpace Registry at the Duraspace website
# and stash the data in a relational database for later.

import sys
from http.client import HTTPSConnection, HTTPResponse, RemoteDisconnected
from bs4 import BeautifulSoup
import csv
import time

DEBUG = True
HOST = 'duraspace.org'
PAGE_URL = '/registry/'

# Connect to the Duraspace DSpace instances registry.
connection = HTTPSConnection(HOST)

# Set up to write CSV to standard output.
# not until 3.7 sys.stdout.reconfigure(newline='')
fieldnames = ['name', 'type', 'country', 'version', 'url']
writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
writer.writeheader()

# Scrape each page
while PAGE_URL is not None :
    # Fetch and parse the page.
    try:
        connection.request('GET', PAGE_URL)
        response = connection.getresponse()
    except RemoteDisconnected:
        print('Connection dropped; retrying', file=sys.stderr)
        connection.close()
        connection = HTTPSConnection(HOST)
        continue

    if response.status in [ 503 ] :
        print(f'Response status {response.status}, reason {response.reason}',
              file=sys.stderr)
        time.sleep(5)
        print('Retrying', file=sys.stderr)
        connection.close()
        connection = HTTPSConnection(HOST)
        continue
    if response.status != 200 :
        print(f'response status = {response.status}, reason = {response.reason}', file=sys.stderr)
        sys.exit(1)

    soup = BeautifulSoup(response, 'html.parser')

    # Find the table of instances.
    section = soup.find('div', id='gv-item-reviewed')
    table = section.find('table')
    tbody = table.find('tbody')
    rows = tbody.find_all('tr')

    # Process each DSpace instance.
    for row in rows:
        # Extract cell values.
        technology = row.find_all(name='td', attrs={'data-label': 'Technology'})[0].get_text().strip()
        if technology.strip().upper() != 'DSPACE' : continue
        institution_name = row.find_all(name='td', attrs={'data-label': 'Organization Name'})[0].a.get_text().strip()
        site_name = row.find_all(name='td', attrs={'data-label': 'Site Name'})[0].get_text().strip()
        institution_type = row.find_all(name='td', attrs={'data-label': 'Type of Organization'})[0].get_text().strip()
        country = row.find_all(name='td', attrs={'data-label': 'Country'})[0].get_text().strip()
        dspace_version = row.find_all(name='td', attrs={'data-label': 'Version'})[0].get_text().strip()
        repo_url_link = row.find_all(name='td', attrs={'data-label': 'Site URL'})[0].a

        # Some rows lack a repository link.  Avoid nullity.
        if repo_url_link is not None :
            repo_url = repo_url_link['href'].strip()
        else :
            repo_url = None

        # Write it all out.
        writer.writerow({'name': institution_name,
                         'type': institution_type,
                         'country': country,
                         'version': dspace_version,
                         'url': repo_url})

    # Find the link to the next page.
    page_menu = soup.find('div', class_='gv-widget-page-links');
    page_next = page_menu.find('a', class_='next');
    if page_next is not None :
        PAGE_URL = page_next['href']
        if DEBUG :
            print('next-page URL: ', PAGE_URL, file=sys.stderr)
    else :
        PAGE_URL = None

# All pages have been seen.
print("That's all!", file=sys.stderr)
